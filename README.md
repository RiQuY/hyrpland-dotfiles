# Hyrpland Dotfiles
Configuration files for an [Hyprland](https://hyprland.org/) installation.

## Description
**TODO**

## Visuals
![Hyprland screenshot](assets/hyprland_screenshot.png?raw=true "Hyprland screenshot")

## Installation
This configuration was created using a openSUSE system with KDE Plasma already installed. The installation is incomplete and still being developed.

## Usage
The configuration is working as is, but it has package requirements not listed yet.

## License
This project is licensed under [GNU Affero General Public License](https://www.gnu.org/licenses/agpl-3.0.en.html). For the full text of the license, see the [LICENSE](LICENSE) file.
